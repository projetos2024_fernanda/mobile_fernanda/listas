import React from "react";
import { View, PanResponder, StyleSheet, Text, Dimensions } from "react-native";
//PanResponder - método para conseguir pegar os gestos dos usuários , Dimensions - dimensão do aparelho
import { useNavigation } from "@react-navigation/native";

const Gesto = ({ navigation }) => {
  const screenWidth = Dimensions.get("window").width;
  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      //identifica o começo do gesto
      onPanResponderMove: (event, gestureState) => {
        //identifica o meio do gesto
        console.log("Movimento X:", gestureState.dx);
        console.log("Movimento Y:", gestureState.dy);
      },
      onPanResponderRelease: (event, gestureState) => {
        //identifica o final do gesto
        if (gestureState.dx > screenWidth / 2) {
          //quando o gesto passar da metade da tela ele volta
          navigation.goBack();
        }
      },
    })
  ).current; // pegar o ponto de cada reinderização (usa com o Useref) 

  return (
    <View style={styles.container}
        {...panResponder.panHandlers}
    >
      <Text style={styles.text}>OI</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "gray",
  },

  text: {
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default Gesto;
